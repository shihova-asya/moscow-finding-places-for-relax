BEGIN;

CREATE TABLE "district" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" varchar(100) NOT NULL
);

CREATE TABLE "place" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "district_id" integer NOT NULL REFERENCES "district" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    "name" varchar(100) NOT NULL,
    "coordinates" varchar(100) NOT NULL,
    "working_hours" varchar(100) NOT NULL,
    "website" varchar(100) NOT NULL,
    "phone" varchar(100) NOT NULL
);

CREATE TABLE "review" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "author_id" integer NULL REFERENCES "user" ("id") ON DELETE SET NULL ON UPDATE CASCADE,
    "place_id" integer NOT NULL REFERENCES "place" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    "text" text NOT NULL,
    "rating" integer NOT NULL
);

CREATE TABLE "tag" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "parent_id" integer NOT NULL REFERENCES "tag" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    "name" varchar(100) NOT NULL
);

CREATE TABLE "underground_station" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "district_id" integer NOT NULL REFERENCES "district" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    "name" varchar(100) NOT NULL,
    "coordinates" varchar(100) NOT NULL
);

CREATE TABLE "user" (
    "user_id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "login" varchar(100) NOT NULL,
    "password" varchar(100) NOT NULL
);

CREATE TABLE "user_tag" (
    "user_id" integer NOT NULL REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    "tag_id" integer NOT NULL REFERENCES "tag" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY ("user_id", "tag_id")
);

CREATE TABLE "place_tag" (
    "place_id" integer NOT NULL REFERENCES "place" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    "tag_id" integer NOT NULL REFERENCES "tag" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY ("place_id", "tag_id")
);

CREATE TABLE "place_underground_station" (
    "place_id" integer NOT NULL REFERENCES "place" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    "underground_station_id" integer NOT NULL REFERENCES "underground_station" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY ("place_id", "underground_station_id")
);

COMMIT;
