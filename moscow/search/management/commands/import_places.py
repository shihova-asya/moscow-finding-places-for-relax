import csv

from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist

from search.models import District, Place

class Command(BaseCommand):
    help = "Imports places from a .csv file"

    def add_arguments(self, parser):
        parser.add_argument("filename")

    def handle(self, *args, **options):
        filename = options["filename"]

        try:
            last_pk = Place.objects.latest("pk").pk
        except ObjectDoesNotExist:
            last_pk = 0

        new_places = []

        with open(filename, encoding="windows-1251") as file:
            reader = csv.DictReader(file, delimiter=";")

            for row in reader:
                name = row["Name"]
                district_name = row["District"]
                phone = row["PublicPhone"]
                x = row["x"]
                y = row["y"]
                coordinates = "[{}, {}]".format(x, y)
                website = ""
                working_hours = ""

                try:
                    website = row["WebSite"]
                    working_hours = row["WorkingHours"]
                except KeyError:
                    pass

                district, created = District.objects.get_or_create(name=district_name)
                last_pk += 1
                place = Place(pk=last_pk, district=district, name=name,
                    coordinates=coordinates, working_hours=working_hours, website=website, phone=phone)
                new_places.append(place)

        Place.objects.bulk_create(new_places)
        self.stdout.write(self.style.SUCCESS("Imported {} places.".format(len(new_places))))
