from django.core.management.base import BaseCommand

from search.models import Place, Underground_station

def parse_coords(coordinates):
    return map(float, coordinates[1:-1].split(", "))

def dist(place, station):
    place_longtitude, place_latitude = parse_coords(place.coordinates)
    station_latitude, station_longtitude = parse_coords(station.coordinates)
    # Moscow is flat enough.
    return ((place_longtitude - station_longtitude)**2 + (place_latitude - station_latitude)**2)**(1/2)

class Command(BaseCommand):
    help = "Sets closest underground stations for each place"

    def add_arguments(self, parser):
        parser.add_argument("--threshold", default="10", type=int,
            help="Stations that are at most THRESHOLD%% farther away than the closest station are included.")
        parser.add_argument("--max-stations", default="3", type=int,
            help="At most MAX_STATIONS stations are included.")

    def handle(self, *args, **options):
        threshold = options["threshold"]
        max_stations = options["max_stations"]

        self.stdout.write("Listing stations...")
        stations = list(Underground_station.objects.all())
        num_places = len(Place.objects.all())
        stations_per_place = []
        ThroughModel = Underground_station.place_set.through
        through_objects = []

        for i, place in enumerate(Place.objects.all()):
            self.stdout.write("\rCalculating closest stations... ({}/{})".format(i + 1, num_places), ending="")
            self.stdout.flush()
            stations.sort(key=lambda station: dist(place, station))

            min_dist = dist(place, stations[0])
            threshold_dist = min_dist * (1 + threshold/100)
            candidates = stations[:max_stations]
            closest_stations = list(filter(lambda station: dist(place, station) <= threshold_dist, candidates))
            stations_per_place.append(len(closest_stations))

            for station in closest_stations:
                through_objects.append(ThroughModel(place_id=place.pk, underground_station_id=station.pk))

        self.stdout.write("")
        self.stdout.write("Saving to database...")
        ThroughModel.objects.bulk_create(through_objects)

        total_stations = sum(stations_per_place)
        average_stations = total_stations / num_places
        self.stdout.write(self.style.SUCCESS("Added {} stations as closest (on average {} per place).".format(total_stations, average_stations)))
