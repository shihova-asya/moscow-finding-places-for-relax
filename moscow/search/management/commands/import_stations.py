import csv

from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from search.models import *

class Command(BaseCommand):
    help = "Imports underground stations from a .csv file"

    def add_arguments(self, parser):
        parser.add_argument("filename")

    def handle(self, *args, **options):
        filename = options["filename"]
        filename = "metro_stations.csv"

        try:
            last_pk = Underground_station.objects.latest("pk").pk
        except ObjectDoesNotExist:
            last_pk = 0

        new_stations = []

        with open(filename, encoding="utf-8") as file:
            reader = csv.DictReader(file, delimiter=",")

            for row in reader:
                district_name = row["district"]
                coordinates = row["coordinates"]
                name = row["name"]
                district, created = District.objects.get_or_create(name=district_name)
                last_pk += 1
                station = Underground_station(pk=last_pk, district=district, name=name, coordinates=coordinates)
                new_stations.append(station)

        Underground_station.objects.bulk_create(new_stations)
        self.stdout.write(self.style.SUCCESS("Imported {} stations.".format(len(new_stations))))
