from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.views.decorators.http import require_POST
from django.utils.http import is_safe_url
from django.db.models import Q
from .models import *

import heapq


def redirect_back(request, *args):
    redirect_to = request.POST.get('next', request.GET.get('next'))

    if redirect_to is not None and is_safe_url(url=redirect_to, host=request.get_host()):
        return redirect(redirect_to)
    else:
        return redirect(*args)


def index(request):
    return render(request, 'index.html', {'stations': Underground_station.objects.all(), 'districts': District.objects.all(), 'tags': Tag.objects.all()})


def log_in(request):
    if request.user.is_authenticated():
        return render(request, 'login.html', {'error': 'Вы уже авторизованы!'})
    if request.method == 'GET':
        return render(request, 'login.html')
    elif request.method == 'POST':
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        if User.objects.filter(username=username).exists():
            logout(request)
            user = authenticate(username=username, password=password)
            if user is None:
                return render(request, 'login.html', {'error': 'Неправильный пароль!'})
            login(request, user)
            return redirect('index')
        else:
            return render(request, 'login.html', {'error': 'Пользователь с таким именем не существует!'})
    return render(request, 'login.html')


@require_POST
def log_out(request):
    logout(request)
    return redirect('index')


def register(request):
    if request.method == 'GET':
        return render(request, 'register.html')
    elif request.method == 'POST':
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        if not User.objects.filter(username=username).exists():
            user = User.objects.create_user(username=username, password=password)
            user.save()
            logout(request)
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('index')
        else:
            return render(request, 'register.html', {'error': 'Такой пользователь уже существует!'})
    return render(request, 'register.html')


def account(request, username):
    subject = get_object_or_404(User, username=username)
    return render(request, 'account.html', {'subject': subject})


def dist(x1, y1, x2, y2):
    return (x1 - x2)**2 + (y1 - y2)**2

def parse_coords(coordinates):
    return map(float, coordinates[1:-1].split(", "))



def main(request):
    selected_stations = request.POST.getlist('stations[]') # Выбранные пользователем станции метро; может быть пустым
    selected_district = request.POST.get('district') # Выбранный пользователем район, может быть None
    selected_tags = request.POST.getlist('tags[]') # Выбранные пользователем теги; может быть пустым
    x, y = request.POST.get('x'), request.POST.get('y') # Координаты пользователя

    q_district = Q(district__name=selected_district)

    q_tags = Q(tags__name__in=selected_tags)

    q_stations = Q(name__in=selected_stations)
    h = []
    q_res = Q()
    if not selected_stations and not selected_district: # Нет заданныйх районов поиска
    
        if x and y: # Есть координаты пользователя
            x = float(x); y = float(y)
            for place in Place.objects.all():
                px, py = parse_coords(place.coordinates)
                heapq.heappush(h, (dist(px, py, x, y), place.coordinates))
            q = [t[1] for t in heapq.nsmallest(100, h)]
            q_res = Q(coordinates__in=q)

    else:
        q_res = q_district
        if selected_stations:
            places = Place.objects.all()
            for stations in Underground_station.objects.filter(q_stations).all():
                y, x = parse_coords(stations.coordinates) # коорднаты для станций в другом порядке
                for place in places:
                    px, py = parse_coords(place.coordinates)
                    heapq.heappush(h, (dist(px, py, x, y), place.coordinates))
                
            q = [t[1] for t in heapq.nsmallest(100, h)]
            q_res = q_res | Q(coordinates__in=q)
            

    if not selected_tags:
        places = Place.objects.filter(q_res).all()[:100]
    else:
        places = Place.objects.filter(q_res & q_tags).all()[:100]
    
    if not places:
        return render(request, 'main.html')

    ystaticmap = 'https://static-maps.yandex.ru/1.x/?l=map&size=650,450&pt='
    i = 1
    for place in places:
        x, y = parse_coords(place.coordinates)
        ystaticmap += str(x) + ',' + str(y) + ',pmrdl'+ str(i) +'~'
        i += 1
    ystaticmap = ystaticmap[:-1]

    return render(request, 'main.html', {'map': True, 'ystaticmap': ystaticmap, 'places': places})


def place(request, place_id):
    place = get_object_or_404(Place, pk=place_id)
    return render(request, 'place.html', {'place': place, 'stations': [station.name for station in place.underground_stations.all()]})

@login_required
def add_review(request, place_id):
    place = get_object_or_404(Place, pk=place_id)

    if request.method == "POST":
        text = request.POST.get('text')
        rating = request.POST.get('rating')
        if rating == '':
            rating = "1"
        review = Review(author=request.user, place=place, text=text, rating=int(rating))
        review.place.before_new_review(review)
        review.save()
        return redirect_back(request, 'place', place.pk)
    else:
        return render(request, 'add_review.html', {'place': place})


@login_required
def change_review(request, review_id):
    review = get_object_or_404(Review, pk=review_id)
    if review.author != request.user:
        raise PermissionDenied

    if request.method == "POST":
        review.text = request.POST.get('text')
        old_rating = review.rating
        review.rating = int(request.POST.get('rating'))
        review.place.before_new_review(review, old_rating)
        review.save()
        return redirect_back(request, 'account', review.author.username)
    else:
        return render(request, 'change_review.html', {'review': review})


@require_POST
@login_required
def del_review(request, review_id):
    review = get_object_or_404(Review, pk=review_id)
    if review.author != request.user:
        raise PermissionDenied

    old_pk = review.pk
    review.delete()
    review.place.after_del_review(review, old_pk)
    return redirect_back(request, 'account', review.author.username)


@login_required
def add_tags(request, place_id):
    place = get_object_or_404(Place, pk=place_id)

    if request.method == "POST":
        tags = request.POST.get('added_tags')
        if tags is not None:
            tags = tags.split("; ")
            for tag in tags:
                sep = tag.find(":")
                if sep == -1:
                    tag, created = Tag.objects.get_or_create(name=tag)
                else:
                    parent = tag[0:sep]
                    tag = tag[sep+1:]

                    if not Tag.objects.filter(name=parent).exists():
                        tag = None
                    elif Tag.objects.filter(name=tag).exists():
                        tag = Tag.objects.get(name=tag)
                    else:
                        tag = Tag(name=tag, parent=Tag.objects.get(name=parent))
                        tag.save()

                if tag is not None:
                    place.tags.add(tag)
                    tag.users.add(request.user)

        return redirect_back(request, 'place', place.pk)
    else:
        return render(request, 'add_tags.html', {'tags': Tag.objects.all(), 'place': place})
