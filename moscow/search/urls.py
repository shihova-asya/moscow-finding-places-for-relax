from django.conf.urls import url
from .views import *

from . import views

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^log_in$', log_in, name='log_in'),
    url(r'^log_out$', log_out, name='log_out'),
    url(r'^register$', register, name='register'),
    url(r'^main$', main, name='main'),
    url(r'^account/(?P<username>[\w.@+-]+)$', account, name='account'),
    url(r'^place/(?P<place_id>[0-9]+)$', place, name='place'),
    url(r'^add_review/(?P<place_id>[0-9]+)$', add_review, name='add_review'),
    url(r'^change_review/(?P<review_id>[0-9]+)$', change_review, name='change_review'),
    url(r'^del_review/(?P<review_id>[0-9]+)$', del_review, name='delete_review'),
    url(r'^add_tags/(?P<place_id>[0-9]+)$', add_tags, name='add_tags'),
]
