from django.db import models
from django.contrib.auth.models import User

class District(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Underground_station(models.Model):
    district = models.ForeignKey(District, on_delete=models.CASCADE, related_name='stations')
    name = models.CharField(max_length=100)
    coordinates = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Tag(models.Model):
    parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=100)
    users = models.ManyToManyField(User)

    def __str__(self):
        return self.name


class Review(models.Model):
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    place = models.ForeignKey("Place", on_delete=models.CASCADE)
    text = models.TextField()
    rating = models.PositiveIntegerField()

    def __str__(self):
        return "Review by {}".format(self.author.username)


class Place(models.Model):
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    coordinates = models.CharField(max_length=100)
    working_hours = models.CharField(max_length=100)
    website = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    tags = models.ManyToManyField(Tag)
    underground_stations = models.ManyToManyField(Underground_station)
    num_reviews = models.PositiveIntegerField(default=0)
    sum_rating = models.PositiveIntegerField(default=0)

    def before_new_review(self, review, old_rating=None):
        try:
            last_review = Review.objects.filter(author=review.author, place=self).latest("pk")
        except Review.DoesNotExist:
            last_review = None

        if old_rating is None:
            if last_review is None:
                self.num_reviews += 1
                self.sum_rating += review.rating
            else:
                self.sum_rating += review.rating - last_review.rating
        else:
            if review.pk == last_review.pk:
                self.sum_rating += review.rating - old_rating

        self.save()

    def after_del_review(self, review, old_pk):
        try:
            last_review = Review.objects.filter(author=review.author, place=self).latest("pk")
        except Review.DoesNotExist:
            self.num_reviews -= 1
            self.sum_rating -= review.rating
        else:
            if old_pk > last_review.pk:
                self.sum_rating -= review.rating - last_review.rating

        self.save()

    def average_rating(self):
        return round(self.sum_rating / self.num_reviews) if self.num_reviews else 0

    def __str__(self):
        return self.name
