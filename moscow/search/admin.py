from django.contrib import admin
from .models import *


admin.site.register(District)
admin.site.register(Place)
admin.site.register(Review)
admin.site.register(Tag)
admin.site.register(Underground_station)
